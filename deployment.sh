#!/usr/bin/env bash

# Sanitize Branch Name
function sanitize_branch {
    echo $1 | sed 's/\//-/g' | sed 's/_/-/g'
}

# Get Commit Hash (7 characthers)
function get_commit_hash {
    echo ${BITBUCKET_COMMIT} | cut -c1-7
}

# Return a valid docker tag name

function get_docker_tag {
    if [[ ! -z "${BITBUCKET_TAG}" ]]; then
        echo ${BITBUCKET_TAG}
    else
        if [[ ! -z "${BITBUCKET_BRANCH}" ]]; then
            sanitize_branch ${BITBUCKET_BRANCH}
        else
            exit 1
        fi
    fi
}

# Authenticate to AWS ECR (Container Registry)
function ecr_authentication {
    eval $(aws ecr get-login --region ${AWS_DEFAULT_REGION} --no-include-email)
}

# Build docker image based and push to AWS ECR

function build_and_push_docker_image {
    ecr_authentication
    docker build -f ./Task-2/Dockerfile -t ${AWS_REGISTRY_URL}:$(get_docker_tag) .
    docker push ${AWS_REGISTRY_URL}:$(get_docker_tag)
}
