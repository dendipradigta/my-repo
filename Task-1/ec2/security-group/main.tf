terraform {
  backend "s3" {
    bucket = "dendi-tf-states"
    key    = "task-1/ec2/security-group/states.tfstate"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  version = "~> 3.0"
}

locals {
  myhome_network_cidr_block = [
    "172.31.4.81/32",
    "52.220.226.144/32"
  ] #My home VPN IP

  myoffice_network_cidr_block = [
    "36.70.141.86/32"
  ] #My office VPN IP

  bitbucket_cidr_block = [
    "34.199.54.113/32",
    "34.232.25.90/32",
    "34.232.119.183/32",
    "34.236.25.177/32",
    "35.171.175.212/32",
    "52.54.90.98/32",
    "52.202.195.162/32",
    "52.203.14.55/32",
    "52.204.96.37/32",
    "34.218.156.209/32",
    "34.218.168.212/32",
    "52.41.219.63/32",
    "35.155.178.254/32",
    "35.160.177.10/32",
    "34.216.18.129/32",
  ] # Bitbucket network (bitbucket.org, api.bitbucket.org, and altssh.bitbucket.org)
}

data "aws_vpc" "dendi_vpc" {
  state = "available"
  filter {
    name   = "tag:Name"
    values = ["dendi-vpc"]
  }
}

//-------------------- Outbound rules -------------------- //
resource "aws_security_group" "allow_ssh_access" {
  vpc_id      = data.aws_vpc.dendi_vpc.id
  name        = "allow-ssh-access"
  description = "Allow SSH Access"

  egress {
    description = "Allow all for outgoing trafics"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

//-------------------- Inbound rules -------------------- //

resource "aws_security_group_rule" "my_home" {
  security_group_id = aws_security_group.allow_ssh_access.id
  description       = "Allow SSH access from My Home Network"

  type        = "ingress"
  protocol    = "tcp"
  from_port   = 22
  to_port     = 22
  cidr_blocks = local.myhome_network_cidr_block
}

resource "aws_security_group_rule" "my_office" {
  security_group_id = aws_security_group.allow_ssh_access.id
  description       = "Allow SSH access from My Office Network"

  type        = "ingress"
  protocol    = "tcp"
  from_port   = 22
  to_port     = 22
  cidr_blocks = local.myoffice_network_cidr_block
}

resource "aws_security_group_rule" "mysql_my_home" {
  security_group_id = aws_security_group.allow_ssh_access.id
  description       = "Allow MySQL access from My Home Network"

  type        = "ingress"
  protocol    = "tcp"
  from_port   = 3306
  to_port     = 3306
  cidr_blocks = local.myhome_network_cidr_block
}


resource "aws_security_group_rule" "ssh_bitbucket" {
  security_group_id = aws_security_group.allow_ssh_access.id
  description       = "Allow SSH access from Bitbucket Network"

  type        = "ingress"
  protocol    = "tcp"
  from_port   = 22
  to_port     = 22
  cidr_blocks = local.bitbucket_cidr_block
}
