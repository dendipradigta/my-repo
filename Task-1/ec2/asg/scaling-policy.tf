resource "aws_autoscaling_policy" "dendi-test-asg-plc" {
  autoscaling_group_name    = "dendi-asg-20211027191653821400000002"
  name                      = "dendi-test-asg-plc"
  policy_type               = "TargetTrackingScaling"
  estimated_instance_warmup = 160

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 45.0
  }
}
