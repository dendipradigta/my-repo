terraform {
  backend "s3" {
    bucket = "dendi-tf-states"
    key    = "task-1/ec2/states.tfstate"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  version = "~> 3.0"
}
//-------------------- Initial resources --------------------//
data "aws_vpc" "dendi_vpc" {
  state = "available"
  filter {
    name   = "tag:Name"
    values = ["dendi-vpc"]
  }
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.dendi_vpc.id
}

data "aws_security_groups" "dendi_allow_ssh" {
  filter {
    name = "group-name"
    values = [
      "allow-ssh-access",
    ]
  }
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.dendi_vpc.id]
  }
}


module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "3.9.0"

  //-------------------- EC2 configuration --------------------//

  name     = "dendi-instance-by-asg"
  key_name = "dendi-personal-sg"

  //-------------------- Launch configuration --------------------//
  lc_name = "dendi-lc"

  image_id        = "ami-0eb3b99a502cb7867"
  instance_type   = "t2.medium"
  security_groups = data.aws_security_groups.dendi_allow_ssh.ids

  ebs_block_device = [
    {
      device_name           = "/dev/xvdz"
      volume_type           = "gp2"
      volume_size           = "10"
      delete_on_termination = true
    },
  ]

  root_block_device = [
    {
      volume_size = "10"
      volume_type = "gp2"
    },
  ]

  //-------------------- Auto scaling group --------------------//
  asg_name                  = "dendi-asg"
  vpc_zone_identifier       = data.aws_subnet_ids.all.ids
  health_check_type         = "EC2"
  min_size                  = 1
  max_size                  = 5
  desired_capacity          = 1
  wait_for_capacity_timeout = 0

  tags = [
    {
      key                 = "provisionedBy"
      value               = "terraform"
      propagate_at_launch = true
    },
    {
      key                 = "Project"
      value               = "task-1"
      propagate_at_launch = true
    },
  ]
}


