terraform {
  backend "s3" {
    bucket = "dendi-tf-states"
    key    = "task-1/vpc/states.tfstate"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  version = "~> 3.0"
}
//-------------------- VPC --------------------//
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name = var.vpc_name
  cidr = var.vpc_cidr

  azs             = var.vpc_azs
  private_subnets = var.vpc_private_subnets
  public_subnets  = var.vpc_public_subnets

  enable_nat_gateway = var.vpc_enable_nat_gateway

  tags = var.vpc_tags
}
