#!/bin/bash
echo "Loging into ECR"
aws ecr get-login-password | sudo docker login --username AWS --password-stdin 566134187357.dkr.ecr.ap-southeast-1.amazonaws.com/task-2

echo "fetching latest image"
docker pull 566134187357.dkr.ecr.ap-southeast-1.amazonaws.com/task-2:latest

echo "Stop current container"
docker stop dendi-task-2
docker rm -f dendi-task-2-old

echo "Rename stopped container to old"
docker rename dendi-task-2 dendi-task-2-old

docker run --name dendi-task-2 -d -p 80:80 566134187357.dkr.ecr.ap-southeast-1.amazonaws.com/task-2:latest